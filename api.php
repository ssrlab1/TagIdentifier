<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	include_once 'TagIdentifier.php';
	
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$certainTag = isset($_POST['certainTag']) ? $_POST['certainTag'] : '';
	$contextSize = isset($_POST['contextSize']) ? $_POST['contextSize'] : 300;
	
	$msg = '';
	if(!empty($text)) {
		$TagIdentifier = new TagIdentifier($contextSize, $certainTag);
		$TagIdentifier->setText($text);
		$TagIdentifier->run();
		$TagIdentifier->saveCacheFiles();

		$result['text'] = $text;
		$result['result'] = $TagIdentifier->getTagList();
		$result['resultArr'] = $TagIdentifier->getTagArray();
		$msg = json_encode($result);
	}
	echo $msg;
?>