<?php
	class TagIdentifier {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $inputText = '';
		private $contextSize = 0;
		private $certainTags = array();
		private $tagArray = array();
		private $tagList = '';
		const BR = "<br>\n";
		const INPUT_TEXT_DEFAULT = "<!DOCTYPE html>\n<html>\n<head>\n<title>Page Title</title>\n</head>\n<body>\n\n<h1>This is a first Heading.</h1>\n<p>This is a first paragraph.</p>\n\n<h1>This is a second Heading.</h1>\n<p>This is a second paragraph.</p>\n\n</body>\n</html>";
		
		function __construct($contextSize, $certainTag) {
			if(!$this->isDigit($contextSize)) {
				$this->contextSize = 300;
			}
			elseif($contextSize >= 0 && $contextSize <= 5040) {
				$this->contextSize = $contextSize;
			}
			else {
				$this->contextSize = 300;
			}
			$this->certainTags = preg_split("/[\s,]+/", trim($certainTag));
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Tag Identifier';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/TagIdentifier/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function setText($text) {
			$this->inputText = $text;
		}
		
		private function isDigit($digit) {
			if(is_int($digit)) return true;
			elseif(is_string($digit)) return ctype_digit($digit);
			else return false;
        }
		
		public function run() {
			if(!empty($this->inputText)) {
				if(count($this->certainTags) === 1 && $this->certainTags[0] === '') {
					$pattern = "/(<(\w+)(\s[^\/>]+)?>.*?<\/\\2>)(.{0," . $this->contextSize . "})/us";
				}
				else {
					$pattern = "/(<(" . implode('|', $this->certainTags) . ")(\s[^>]+)?>.*?<\/\\2>)(.{0," . $this->contextSize . "})/us";
				}
				$offset = 0;
				while(true) {
					if(preg_match($pattern, $this->inputText, $match, PREG_OFFSET_CAPTURE, $offset) === 1) {
						if(isset($match[0]) && isset($match[1]) && isset($match[2])) {
							$contextLeft = substr($this->inputText, 0, $match[0][1]);
							$contextLen = mb_strlen($contextLeft, 'UTF-8');
							if($contextLen > $this->contextSize) {
								$startPos = $contextLen - $this->contextSize;
								$contextLeft = mb_substr($contextLeft, $startPos, $this->contextSize, 'UTF-8');
							}
							$this->tagArray[$match[2][0]][] = htmlspecialchars($contextLeft) . '<font color="red">' . htmlspecialchars($match[1][0]) . '</font>' . htmlspecialchars($match[4][0]);
						}
						$offset = $match[2][1];
					}
					else {
						break;
					}
				}
			}
			foreach($this->tagArray as $tag => $tagFool) {
				$tag = "<$tag>";
				$cnt = count($tagFool);
				$this->tagList .= '<b><font color="red">' . htmlspecialchars($tag) . "</font></b> ($cnt)<br><br>";
				$this->tagList .= implode('<br><br>', $tagFool) . '<br><br><br>';
			}
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'TagIdentifier';
			$sendersName = 'Tag Identifier';
			$senders_email = "corpus.by@gmail.com";
			$recipient = "corpus.by@gmail.com";
			$subject = "TagIdentifier from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR;
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$text_length = mb_strlen($this->inputText);
			$pages = round($text_length/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->inputText);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$url = $root . "/showCache.php?s=TagIdentifier&t=in&f=$filename";
			if(mb_strlen($cache_text)) {
				$mail_body .= "Тэкст ($text_length сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cache_text, $matches);
				$str = str_replace("\r\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 1000) {
					$mail_body .= '<blockquote><i>' . htmlspecialchars($str) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mail_body .= '<blockquote><i>' . htmlspecialchars(mb_substr($str, 0, 1000)) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mail_body .= self::BR;

			$mail_body .= 'Карыстальнік абраў наступныя наладкі:' . self::BR;
			$mail_body .= '<blockquote><i>';
			$mail_body .= '– колькасць сімвалаў кантэксту: <b>' . $this->contextSize . '</b>';
			$mail_body .= '</i></blockquote>' . self::BR;
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->tagList);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$url = $root . "/showCache.php?s=TagIdentifier&t=out&f=$filename";
			if(mb_strlen($cache_text)) {
				$mail_body .= "Выніковы спіс пачынаецца так:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cache_text, $matches);
				$mail_body .= '<blockquote><i>' . str_replace("\r\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}

			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>\r\n";
			mail($recipient, $subject, $mail_body, $header);
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getTagList() {
			return $this->tagList;
		}
		
		public function getTagArray() {
			return $this->tagArray;
		}
	}
?>