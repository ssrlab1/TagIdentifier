######################
##### TEST TEMPLATE #####
######################
# Activation
# input: Text
# input: Certain Tag
# input: Context Size
# output: Required result

######################
###### REAL TESTS #######
######################
# Activation
1
# input: Text
<!DOCTYPE html>\n<html>\n<head>\n<title>Page Title</title>\n</head>\n<body>\n<h1>This is a first Heading.</h1>\n<p>This is a first paragraph.</p>\n<h1>This is a second Heading.</h1>\n<p>This is a second paragraph.</p>\n</body>\n</html>
# input: Certain Tag
title
# input: Context Size
10
# output: Required result
{"title":["\\n&lt;head&gt;\\n<font color=\"red\">&lt;title&gt;Page Title&lt;\/title&gt;<\/font>\\n&lt;\/head&gt;\\"]}

# 1+
# Activation
1
# input: Text
<!DOCTYPE HTML><html><head><title>Тег BUTTON</title></head><body><p style="text-align: center"><button>Кнопка с текстом</button><button>Вторая кнопка</button></p></body></html>
# input: Certain Tag
button
# input: Context Size
1000
# output: Required result
{"button":["&lt;!DOCTYPE HTML&gt;&lt;html&gt;&lt;head&gt;&lt;title&gt;Тег BUTTON&lt;\/title&gt;&lt;\/head&gt;&lt;body&gt;&lt;p style=&quot;text-align: center&quot;&gt;<font color=\"red\">&lt;button&gt;Кнопка с текстом&lt;\/button&gt;<\/font>&lt;button&gt;Вторая кнопка&lt;\/button&gt;&lt;\/p&gt;&lt;\/body&gt;&lt;\/html&gt;","&lt;!DOCTYPE HTML&gt;&lt;html&gt;&lt;head&gt;&lt;title&gt;Тег BUTTON&lt;\/title&gt;&lt;\/head&gt;&lt;body&gt;&lt;p style=&quot;text-align: center&quot;&gt;&lt;button&gt;Кнопка с текстом&lt;\/button&gt;<font color=\"red\">&lt;button&gt;Вторая кнопка&lt;\/button&gt;<\/font>&lt;\/p&gt;&lt;\/body&gt;&lt;\/html&gt;"]}

# 2+
# Activation
1
# input: Text
<!DOCTYPE html><html> <head><title>canvas</title><meta charset="utf-8"><script> window.onload = function() {var drawingCanvas = document.getElementById('smile');if(drawingCanvas && drawingCanvas.getContext) {var context = drawingCanvas.getContext('2d');// Рисуем окружность context.strokeStyle = "#000";context.fillStyle = "#fc0";context.beginPath();context.arc(100,100,50,0,Math.PI*2,true);context.closePath();context.stroke();context.fill();// Рисуем левый глаз context.fillStyle = "#fff";context.beginPath();context.arc(84,90,8,0,Math.PI*2,true);context.closePath();context.stroke();context.fill();// Рисуем правый глаз context.beginPath();context.arc(116,90,8,0,Math.PI*2,true);context.closePath();context.stroke();context.fill();// Рисуем ротcontext.beginPath();context.moveTo(70,115);context.quadraticCurveTo(100,130,130,115);context.quadraticCurveTo(100,150,70,115); context.closePath();context.stroke();context.fill();}}</script></head><body><canvas id="smile" width="200" height="200"><p>Ваш браузер не поддерживает рисование.</p></canvas></body></html>
# input: Certain Tag
script
# input: Context Size
20
# output: Required result
{"script":["eta charset=&quot;utf-8&quot;&gt;<font color=\"red\">&lt;script&gt; window.onload = function() {var drawingCanvas = document.getElementById('smile');if(drawingCanvas &amp;&amp; drawingCanvas.getContext) {var context = drawingCanvas.getContext('2d');\/\/ Рисуем окружность context.strokeStyle = &quot;#000&quot;;context.fillStyle = &quot;#fc0&quot;;context.beginPath();context.arc(100,100,50,0,Math.PI*2,true);context.closePath();context.stroke();context.fill();\/\/ Рисуем левый глаз context.fillStyle = &quot;#fff&quot;;context.beginPath();context.arc(84,90,8,0,Math.PI*2,true);context.closePath();context.stroke();context.fill();\/\/ Рисуем правый глаз context.beginPath();context.arc(116,90,8,0,Math.PI*2,true);context.closePath();context.stroke();context.fill();\/\/ Рисуем ротcontext.beginPath();context.moveTo(70,115);context.quadraticCurveTo(100,130,130,115);context.quadraticCurveTo(100,150,70,115); context.closePath();context.stroke();context.fill();}}&lt;\/script&gt;<\/font>&lt;\/head&gt;&lt;body&gt;&lt;canvas"]}

# 3+
# Activation
1
# input: Text
<!DOCTYPE html><html><head><title>datalist</title></head><body><p>Выберите любимого персонажа:</p><p><input list="character"><datalist id="character"><option value="Чебурашка"></option><option value="Крокодил Гена"></option><option value="Шапокляк"></option></datalist></p></body></html>
# input: Certain Tag
option
# input: Context Size
25
# output: Required result
{"option":["&lt;datalist id=&quot;character&quot;&gt;<font color=\"red\">&lt;option value=&quot;Чебурашка&quot;&gt;&lt;\/option&gt;<\/font>&lt;option value=&quot;Крокодил Г","lue=&quot;Чебурашка&quot;&gt;&lt;\/option&gt;<font color=\"red\">&lt;option value=&quot;Крокодил Гена&quot;&gt;&lt;\/option&gt;<\/font>&lt;option value=&quot;Шапокляк&quot;&gt;","&quot;Крокодил Гена&quot;&gt;&lt;\/option&gt;<font color=\"red\">&lt;option value=&quot;Шапокляк&quot;&gt;&lt;\/option&gt;<\/font>&lt;\/datalist&gt;&lt;\/p&gt;&lt;\/body&gt;&lt;\/h"]}

# 4+
# Activation
1
# input: Text
<!DOCTYPE html><html><head><meta charset="utf-8"><title>output</title></head><body><form oninput="result.value=(cm.value/2.54).toFixed(2)"><p>Введите длину в сантиметрах: <input type="number" name="cm" autofocus></p><p>Длина в дюймах: <output name="result">0</output></p></form></body></html>
# input: Certain Tag
title output
# input: Context Size
25
# output: Required result
{"title":["ad&gt;&lt;meta charset=&quot;utf-8&quot;&gt;<font color=\"red\">&lt;title&gt;output&lt;\/title&gt;<\/font>&lt;\/head&gt;&lt;body&gt;&lt;form oninpu"],"output":["s&gt;&lt;\/p&gt;&lt;p&gt;Длина в дюймах: <font color=\"red\">&lt;output name=&quot;result&quot;&gt;0&lt;\/output&gt;<\/font>&lt;\/p&gt;&lt;\/form&gt;&lt;\/body&gt;&lt;\/html&gt;"]}

# 5+
# Activation
1
# input: Text
<body><p><ruby> 字 <rp>(</rp><rt>zì</rt><rp>)</rp></ruby></p></body>
# input: Certain Tag
ruby rp p
# input: Context Size
15
# output: Required result
{"p":["&lt;body&gt;<font color=\"red\">&lt;p&gt;&lt;ruby&gt; 字 &lt;rp&gt;(&lt;\/rp&gt;&lt;rt&gt;zì&lt;\/rt&gt;&lt;rp&gt;)&lt;\/rp&gt;&lt;\/ruby&gt;&lt;\/p&gt;<\/font>&lt;\/body&gt;"],"ruby":["&lt;body&gt;&lt;p&gt;<font color=\"red\">&lt;ruby&gt; 字 &lt;rp&gt;(&lt;\/rp&gt;&lt;rt&gt;zì&lt;\/rt&gt;&lt;rp&gt;)&lt;\/rp&gt;&lt;\/ruby&gt;<\/font>&lt;\/p&gt;&lt;\/body&gt;"],"rp":["dy&gt;&lt;p&gt;&lt;ruby&gt; 字 <font color=\"red\">&lt;rp&gt;(&lt;\/rp&gt;<\/font>&lt;rt&gt;zì&lt;\/rt&gt;&lt;rp&gt;","\/rp&gt;&lt;rt&gt;zì&lt;\/rt&gt;<font color=\"red\">&lt;rp&gt;)&lt;\/rp&gt;<\/font>&lt;\/ruby&gt;&lt;\/p&gt;&lt;\/bo"]}

# 6+
# Activation
1
# input: Text
<!DOCTYPE HTML><html><head><meta charset="utf-8"><title>Тег Q</title><style>q {font-family: Times, serif; /* Шрифт с засечками */ font-style: italic; /* Курсивное начертание текста */color: #008; /* Цвет текста */}</style></head><body><p>Станислав Лец утверждал: <q>Чаще всего выход там, где был вход</q>.</p></body></html>
# input: Certain Tag
q
# input: Context Size
5
# output: Required result
{"q":["дал: <font color=\"red\">&lt;q&gt;Чаще всего выход там, где был вход&lt;\/q&gt;<\/font>.&lt;\/p&gt;"]}

# 1- Сэрвіс не знаходзіць адзінкавыя тэгі - як у фармаце HTML, так і ў фармаце XHTML. Спіс адзінкавых тэгаў HTML можна знайсці тут: http://gabdrahimov.ru/html-tegi-odinochnye
# Activation
1
# input: Text
<b><br /></b>
# input: Certain Tag
br
# input: Context Size
10
# output: Required result
{"br":["&lt;bq&gt;<font color=\"red\">&lt;br \/&gt;<\/font>&lt;\/b&gt;"]}

# 2- Пры заданні "знайсці ўсе тэгі" сэрвіс не знаходзіць тэг а, яго збівае з панталыку сімвал "/". Пры заданні "знайсці толькі тэг а" усё працуе правільна.
# Activation
1
# input: Text
<p><a href="/"></a></p>
# input: Certain Tag
null
# input: Context Size
10
# output: Required result
{"a":["&lt;p&gt;<font color=\"red\">&lt;a href=&quot;\/&quot;&gt;&lt;\/a&gt;<\/font>&lt;\/p&gt;"],"p":["<font color=\"red\">&lt;p&gt;&lt;a href=&quot;\/&quot;&gt;&lt;\/a&gt;&lt;\/p&gt;<\/font>"]}

# 3- Сэрвіс лічыць закрываючым тэгам заўжды толькі першы закрываючы тэг са спісу, хаця рэальна пары тэгаў маюць іншы выгляд.
# Activation
1
# input: Text
<div><div><div></div></div></div>
# input: Certain Tag
null
# input: Context Size
100
# output: Required result
{"div":["<font color=\"red\">&lt;div&gt;&lt;div&gt;&lt;div&gt;&lt;\/div&gt;&lt;\/div&gt;&lt;\/div&gt;<\/font>","&lt;div&gt;<font color=\"red\">&lt;div&gt;&lt;div&gt;&lt;\/div&gt;&lt;\/div&gt;<\/font>&lt;\/div&gt;","&lt;div&gt;&lt;div&gt;<font color=\"red\">&lt;div&gt;&lt;\/div&gt;<\/font>&lt;\/div&gt;&lt;\/div&gt;"]}

# 4- Спецыяльна прапанаваны код з памылкай (адзін div не закрыты). Сэрвіс паўтарае памылку з тэста №3-, пры гэтым знаходзіць ажно чатыры пары!
# Activation
1
# input: Text
<div><div><div></div><div></div></div>
# input: Certain Tag
div
# input: Context Size
10
# output: Required result
{"div":["&lt;div&gt;<font color=\"red\">&lt;div&gt;&lt;div&gt;&lt;\/div&gt;&lt;div&gt;&lt;\/div&gt;&lt;\/div&gt;<\/font>","&lt;div&gt;&lt;div&gt;<font color=\"red\">&lt;div&gt;&lt;\/div&gt;<\/font>&lt;div&gt;&lt;\/div&gt;&lt;\/div&gt;","&lt;div&gt;&lt;div&gt;&lt;div&gt;&lt;\/div&gt;<font color=\"red\">&lt;div&gt;&lt;\/div&gt;<\/font>&lt;\/div&gt;"]}

# 5- У дадзеным тэсце сэрвіс знаходзіць камбінацыю <code<>, якая дакладна не можа быць тэгам. Плюс сістэма тэставання ў input паказвае не тое, што было ўведзена.
# Activation
1
# input: Text
<code< class="no-buttons">&lt;q&gt;Текст&lt;/q&gt;</code<>
# input: Certain Tag
null
# input: Context Size
10
# output: Required result
[]

# 6- Па-першае, у тэгу, як правіла, ужываюцца толькі сімвалы ангельскага алфавіта. Па-другое, у вывадзе знойдзены тэг адлюстраваны... дзіўна.
# Activation
1
# input: Text
<مَطَرٍ></مَطَرٍ>
# input: Certain Tag
null
# input: Context Size
10
# output: Required result
[]