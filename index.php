<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'TagIdentifier.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = TagIdentifier::loadLanguages();
	TagIdentifier::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo TagIdentifier::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', TagIdentifier::INPUT_TEXT_DEFAULT)); ?>";
			
			function escapeHtml(text) {
				var map = {
					'&': '&amp;',
					'<': '&lt;',
					'>': '&gt;',
					'"': '&quot;',
					"'": '&#039;'
				};
				return text.replace(/[&<>"']/g, function(m) { return map[m]; });
			}
			
			function ajaxRequest() {
				$.ajax({
					type: 'POST',
					url: 'https://corpus.by/TagIdentifier/api.php',
					data: {
						'localization': '<?php echo $lang; ?>',
						'text': $('textarea#inputTextId').val(),
						'certainTag': $('input#certainTagId').val(),
						'contextSize': $('input#contextSizeId').val()
					},
					success: function(msg) {
						var result = jQuery.parseJSON(msg);
						var output = '';
						if(result.resultArr) {
							output += Object.keys(result.resultArr).reduce(
								function(previousValue, currentValue) {
									return(previousValue + '<b><font color="red">' + escapeHtml('<' + currentValue + '>') + "</font></b> (" + result.resultArr[currentValue].length + ")<br><br>"
										+ result.resultArr[currentValue].join('<br><br>') + '<br><br><br>'
									);
								},
								"",
							);
						}
						$('#resultId').html(output);
						$('#resultBlockId').show('slow');
					},
					error: function() {
						$('#resultId').html('ERROR');
					}
				});
			}
			$(document).ready(function () {
				$('button#MainButtonId').click(function(){
					$('#resultId').html('...');
					ajaxRequest();				
				});
				$(document).on('keypress', 'input[type=text]', function (e) {
					if (e.which == 13) {
						$('#resultId').html('...');
						ajaxRequest();
						return false;
					}
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/TagIdentifier/?lang=<?php echo $lang; ?>"><?php echo TagIdentifier::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo TagIdentifier::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo TagIdentifier::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = TagIdentifier::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . TagIdentifier::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo TagIdentifier::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo TagIdentifier::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo TagIdentifier::showMessage('input'); ?></h3>
							</div>									
							<div class="panel-body">
								<p>
									<textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo TagIdentifier::INPUT_TEXT_DEFAULT; ?></textarea>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<span class="bold"><?php echo TagIdentifier::showMessage('certain tag'); ?></span>
					</div>
					<div class="col-md-8">
						<input type="text" class="input-primary" id="certainTagId" name="certainTag" size="15" value="p h1">
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-4">
						<span class="bold"><?php echo TagIdentifier::showMessage('context size'); ?></span>
					</div>
					<div class="col-md-8">
						<input type="text" class="input-primary" id="contextSizeId" name="contextSize" size="15" value="300">
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo TagIdentifier::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo TagIdentifier::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo TagIdentifier::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/TagIdentifier" target="_blank"><?php echo TagIdentifier::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/TagIdentifier/-/issues/new" target="_blank"><?php echo TagIdentifier::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo TagIdentifier::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo TagIdentifier::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo TagIdentifier::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php TagIdentifier::sendErrorList($lang); ?>